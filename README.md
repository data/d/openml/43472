# OpenML dataset: Dogecoin-Historical-Data

https://www.openml.org/d/43472

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Introduction
Dogecoin is an open source peer-to-peer digital currency, favored by Shiba Inus worldwide. It is qualitatively more fun while being technically nearly identical to its close relative Bitcoin. This dataset contains its historical stock price in USD on a daily frequency starting from 17 September 2014.
For more information refer to https://dogecoin.com/

Credits
Image Credits: Unsplash - claybanks

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43472) of an [OpenML dataset](https://www.openml.org/d/43472). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43472/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43472/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43472/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

